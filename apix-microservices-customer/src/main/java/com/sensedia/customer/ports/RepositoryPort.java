package com.sensedia.customer.ports;

import com.sensedia.customer.domains.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryPort extends CrudRepository<Customer, String> {}