package com.sensedia.commons.errors.exceptions;

import org.springframework.http.HttpStatus;

public class IntegrationException extends ApplicationException {

  public IntegrationException() {
    this(null, null, null, null);
  }

  public IntegrationException(String detail) {
    this(detail, null, null, null);
  }

  public IntegrationException(String detail, String type) {
    this(detail, type, null, null);
  }

  public IntegrationException(String detail, String type, String title) {
    this(detail, type, title, null);
  }

  public IntegrationException(Throwable cause) {
    this(null, cause);
  }

  public IntegrationException(String detail, Throwable cause) {
    this(detail, null, cause);
  }

  public IntegrationException(String detail, String type, Throwable cause) {
    this(detail, type, null, cause);
  }

  public IntegrationException(String detail, String type, String title, Throwable cause) {
    super(HttpStatus.SERVICE_UNAVAILABLE, detail, type, title, cause);
  }
}
